#ifndef __DUMMY_HH__
#define __DUMMY_HH__

#include <iostream>
#include <string>
#include <bits/stdc++.h>
class Dummy {
public:
    // constructor
    Dummy();
    // destructor
    ~Dummy();
    // public member functions
  float add(float x, float y);
  float subtract(float x, float y);
  float multiply(float x, float y);
  std::string re_string(std::string x);
  void multistr_re(std::vector<std::string> x);
  void GetName(std::string x);
  void GetAddress(std::string x);
};

#endif
