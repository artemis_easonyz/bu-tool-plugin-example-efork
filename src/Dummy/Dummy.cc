#include <Dummy/Dummy.hh>
#include <AddressTable.hh>
#include <fstream>
#include <string>
#include <iostream>
#include <bits/stdc++.h>

/// constructor 
Dummy::Dummy() {
    std::cout << "Hello World - from Dummy" << std::endl;
}

// deconstructor 
Dummy::~Dummy() {
    std::cout << "object is being destroyed, destructor called" << std::endl;
}

float Dummy::add(float x, float y) {
    float res;
    res = x + y;
    std::cout << x << " + " << y << " = " << res << std::endl;
    return res;
}

float Dummy::subtract(float x, float y) {
    float res;
    res = x - y;
    std::cout << x << " - " << y << " = " << res << std::endl;
    return res;
}

float Dummy::multiply(float x, float y) {
    float res;
    res = x * y;
    std::cout << x << " * " << y << " = " << res << std::endl;
    return res;
}

std::string Dummy::re_string(std::string x) {
  int n = x.length();
  std::string redstring = x;
  for (int i = 0; i < n / 2; i++)
    std::swap(redstring[i], redstring[n-i-1]); //in order to avoid confusing about varible names
  return redstring;
}

void Dummy::multistr_re(std::vector<std::string> x){
  
  int inputlen = x.size();
  for (int i=inputlen-1; i>=0; i--){
    std::string redonestr = x[i];
    int strlen = redonestr.length();
    for (int j=0; j<strlen/2; j++)
      std::swap(redonestr[j], redonestr[strlen-j-1]);
    std::cout<<redonestr<<" ";
  }
  std::cout<<std::endl;
}
  
void Dummy::GetName(std::string x) {
  AddressTable TheTable(x);
  std::vector<std::string> TableNames = TheTable.GetName();
  for (size_t i = 0; i < TableNames.size(); i++) {
    std::cout<< TableNames[i] << std::endl;
  }
}

void Dummy::GetAddress(std::string x) {
  AddressTable TheTable2(x);
  std::vector<std::string> TableAddress = TheTable2.GetAddress();
  for (size_t i = 0; i < TableAddress.size(); i++) {
    std::cout<< TableAddress[i] << std::endl;
  }
}
